const _ = require('lodash');

function flattenTree(rootArgs) {
    let queue = [].concat(rootArgs);
    let result = [];
    while (queue.length > 0) {
        const activity = queue.shift();
        for (var key in activity) {
            if (activity.hasOwnProperty(key)) {
                // If the activity has children and isn't what we're looking for, look in its children
                if (key === '@if') {
                    if (_.has(activity[key], 'then')) {
                        addArgs(queue, [activity[key]['then']]);
                        continue;
                    }
                    if (_.has(activity[key], 'else')) {
                        addArgs(queue, [activity[key]['else']]);
                        continue;
                    }
                }
                if (key === '@times' || key === '@block' || key === '@while' || key == '@forEach') {
                    const newArgs = activity[key]['args'];
                    if (newArgs) {
                        addArgs(queue, newArgs);
                        // Keep the wrapping blocks
                        // continue;
                    }
                }
                result.push(key);
            }
        }
    }
    return result;
}

function addArgs(queue, newArgsArray) {
    if (_.isArray(newArgsArray)) {
        Array.prototype.unshift.apply(queue, newArgsArray.filter(r => _.isObject(r)));
    }
}

module.exports = flattenTree;