const fs = require('fs');
const path = require('path');
const _ = require('lodash');
const util = require('util');

// https://github.com/cazala/synaptic/wiki/Normalization-101
// Array of words into normalized things
function normalizeWordsArray(wordsArray) {
    const distinctWords = _.uniq(wordsArray);
    const width = distinctWords.length;
    const result = new Map();
    distinctWords.forEach((r, i) => {
        const translation = fillWithZeroes(width);
        translation[i] = 1;
        result.set(r, {
            index: i,
            translation
        });
    });
    return result;
}

// fillWithZeroes(5) => [ 0, 0, 0, 0, 0]
function fillWithZeroes(length) {
    const result = [];
    for (let i = 0; i < length; i++) {
        result.push(0);
    }
    return result;
}

// Load files, copied from automation-core/collectnative
function fromDir(startPath, filter, callback) {
    if (!fs.existsSync(startPath)) {
        console.log('no dir: ', startPath);
        return;
    }
    const files = fs.readdirSync(startPath);
    files.forEach((file) => {
        const filename = path.join(startPath, file);
        const stat = fs.lstatSync(filename);
        if (filename.indexOf(filter) >= 0) {
            callback(filename);
        }
    });
}

// Take in a normalized array map, array[array] of the activities, segmentation length, get back an array of { input: [], output: []}
// Return 
function createTrainingSet(normalizedMap, incomingData, segmentLength) {
    const result = [];

    // Incoming data is split into arrays of workflows
    incomingData.forEach(workflow => {
        workflow.forEach((r, index) => {
            // Ignore the last activity of the workflow, since it doesn't lead toward anything
            if (index === workflow.length - 1) {
                return;
            }
            // Get the current activity and the next
            const input = normalizedMap.get(r).translation;
            const output = normalizedMap.get(workflow[index + 1]).translation;
            result.push({ input, output });
        })
    });
    return result;
}

module.exports = {
    normalizeWordsArray,
    fillWithZeroes,
    fromDir,
    createTrainingSet
}