const commander = require('commander');
const flattenTree = require('./flattenTree');
const { Architect, Trainer, Layer } = require('synaptic');
const _ = require('lodash');
const util = require('util');
const {
    dec2bin,
    normalizeWordsArray,
    fillWithZeroes,
    fromDir,
    createTrainingSet
} = require('./utils');

const collect = (val, memo) => {
    memo.push(val);
    return memo;
};

function collectWorkflowsFromPaths(paths) {
    const workflows = [];
    paths.forEach(p => {
        fromDir(p, '.json', (file) => {
            const workflow = require(file);
            workflows.push(workflow);
        });
    });
    return workflows;
}

function findActivityByIndex(index, normalizedMap) {
    let activity;
    normalizedMap.forEach((v, k, m) => {
        if (v.index === index) {
            activity = k;
        }
    });
    return activity;
}

commander.usage('[options]')
    .option('-p, --path [path]', 'Path to where the previous workflows are', collect, [])
    .parse(process.argv);

if (commander.path) {
    // Collect the workflows
    const workflows = collectWorkflowsFromPaths(commander.path);
    const series = [];
    // Collect all the steps per workflow, akin to creating a "page" of a book and then linking them up in a series
    workflows.forEach(r => {
        const result = flattenTree(r['@workflow']['args']);
        series.push(result);
    });

    // Get all of our activities
    const actions = _.flatten(series);
    // Create a unique representation of each activity, normalized
    const normalizedMap = normalizeWordsArray(actions);

    const trainingSet = createTrainingSet(normalizedMap, series, 5);

    const grouppedCount = _.chain(actions).groupBy(r => r).toPairs().map(r => { return { activity: r[0], count: r[1].length } }).value();

    console.log('Number of workflows', series.length);
    console.log('Total number of activities', actions.length);
    console.log('Unique actions', normalizedMap.size);
    console.log('Training set size', trainingSet.length);
    // console.log('Activity counts', util.inspect(grouppedCount, false, null));
    // console.log('Training set', util.inspect(trainingSet));

    // Documentation on training
    const lstmOptions = {
        peepholes: Layer.connectionType.ALL_TO_ALL,
        hiddenToHidden: false,
        outputToHidden: false,
        outputToGates: false,
        inputToOutput: true,
    };

    const trainOptions = {
        rate: 0.1,
        iterations: 100,
        error: 0.005,
        cost: null,
        crossValidate: null,
        log: 1
    };
    // Input and output sizes need to be same as the training size
    // Haven't experimented much with the number of nodes, but there's a great discussion here
    // Going ridiculously high
    // https://stats.stackexchange.com/questions/181/how-to-choose-the-number-of-hidden-layers-and-nodes-in-a-feedforward-neural-netw
    const lstm = new Architect.LSTM(normalizedMap.size, 10, normalizedMap.size, lstmOptions);
    console.log('LTSM network created.');
    const trainingResults = lstm.trainer.train(trainingSet, trainOptions);
    console.log('Training complete.');

    // Some series of activities
    const testData = [
        // Random steps I made up
        ['@console', '@pluck', '@distinct', '@console', '@saveData'],
        ['@mouseClick', '@mouseClick', '@mouseClick'],
        // Workflow 4 - "Navigate to Schedule Desktop" next step is "@clickNativeText"
        ['@block', '@console', '@clickNativeText'],
        // Workflow 4 - "Lookup Patients and Grab Additional Data" next step is "@mouseClick"
        ['@block', '@forEach', '@block', '@windowDimensions'],
        // Same as above, additional steps here, next step is "@sendKeys"
        ['@block', '@forEach', '@block', '@windowDimensions', '@getWindow', '@delay']
    ];


    // Example code source https://jsfiddle.net/k23zbf0f/5/

    // Disabled code, but it spits out next activity for the training sets.
    testData.forEach(row => {
        let nextActivity;
        row.forEach((r, index) => {
            const lstmResult = lstm.activate(normalizedMap.get(r).translation);
            // Report the last activation
            if (index === row.length - 1) {
                // Finds the largest value in the array
                const max = Math.max.apply(null, lstmResult);
                const indexOfConfidence = lstmResult.indexOf(max);
                nextActivity = findActivityByIndex(indexOfConfidence, normalizedMap);
            }
        });
        console.log('Entry for requesting next activity', row);
        console.log('Suggestion', nextActivity);
        lstm.clear();
    });
    console.log('Finished.');
    process.exit();
}
